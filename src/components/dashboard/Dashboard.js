import { Avatar, Card, Col, Row } from "antd";
import axios from "axios";
import React, { Component } from "react";
import { Bar } from "react-chartjs-2";
import Clock from "react-live-clock";
const { Meta } = Card;
// temporary dataset for chart
const state = {
  labels: [],
  datasets: [
    {
      label: "Equipment Type",
      backgroundColor: "rgba(75,192,192,1)",
      borderColor: "rgba(0,0,0,1)",
      borderWidth: 2,
      data: [],
    },
  ],
};

export class Dashboard extends Component {
  state = {
    cardOneValue: "",
    cardTwoValue: "",
    allData: {},
    calculatedData: {},
  };

  componentDidMount() {
    this.getCardOneData();
    this.getCardTwoData();
    this.getAllData();
  }
  // get Non-Operational data count
  getCardOneData() {
    axios
      .get(
        `https://cors-anywhere.herokuapp.com/http://ivivaanywhere.ivivacloud.com/api/Asset/Asset/All`,
        {
          params: {
            OperationalStatus: "Non-Operational",
            apikey: "SC:demo:64a9aa122143a5db",
            max: 99,
          },
        }
      )
      .then((res) => {
        const cardOneValue = res.data.length;
        this.setState({ cardOneValue });
      });
  }
  // get Operational data count
  getCardTwoData() {
    axios
      .get(
        `https://cors-anywhere.herokuapp.com/http://ivivaanywhere.ivivacloud.com/api/Asset/Asset/All`,
        {
          params: {
            OperationalStatus: "Operational",
            apikey: "SC:demo:64a9aa122143a5db",
            max: 99,
          },
        }
      )
      .then((res) => {
        const cardTwoValue = res.data.length;
        this.setState({ cardTwoValue });
      });
  }
  // get complect apiCall
  getAllData() {
    axios
      .get(
        `https://cors-anywhere.herokuapp.com/http://ivivaanywhere.ivivacloud.com/api/Asset/Asset/All`,
        {
          params: {
            apikey: "SC:demo:64a9aa122143a5db",
            max: 99,
          },
        }
      )
      // calculate duplicating elements and save
      .then((res) => {
        const allData = res.data;
        var rez = {};
        allData.forEach(function (item) {
          rez[item.AssetCategoryID]
            ? rez[item.AssetCategoryID]++
            : (rez[item.AssetCategoryID] = 1);
        });
        console.log(rez);
        state.labels = Object.keys(rez);
        state.datasets[0].data = Object.values(rez);
      });
  }
  render() {
    return (
      <div className="w-full p-20">
        <Row gutter={24}>
          {/* non-operational card */}
          <Col span={8}>
            <Card
              hoverable={true}
              style={{
                // marginLeft: "2%",
                height: "200px",
                outline: "0 !important",
                border: "0 !important",
                boxShadow: "none !important",
                borderTop: "3px solid red",
                //   borderRadius: "20px",
              }}
            >
              <Meta
                avatar={
                  <Avatar
                    size={150}
                    src="https://static.iviva.com/images/iviva_AboutUs-09.png"
                  />
                }
                style={{ fontSize: 20 }}
                title={this.state.cardOneValue}
                description="Number of non-operational equipment"
              />
            </Card>
          </Col>
          {/* non-operational card */}
          <Col span={8}>
            <Card
              hoverable={true}
              style={{
                // marginLeft: "2%",
                height: "200px",
                outline: "0 !important",
                border: "0 !important",
                boxShadow: "none !important",
                borderTop: "3px solid red",
                //   borderRadius: "20px",
              }}
            >
              <Meta
                avatar={
                  <Avatar
                    size={150}
                    src="https://static.iviva.com/images/iviva_AboutUs-10.png"
                  />
                }
                style={{ fontSize: 20 }}
                title={this.state.cardTwoValue}
                description="Number of operational equipment"
              />
            </Card>
          </Col>
          <Col span={8}>
            <Card
              hoverable={true}
              style={{
                height: "200px",
                outline: "0 !important",
                border: "0 !important",
                boxShadow: "none !important",
                borderTop: "3px solid red",
              }}
            >
              {/* Clock */}
              <Row gutter={24}>
                <Col span={12}>
                  <Clock
                    style={{
                      fontSize: "50px",
                      fontWeight: "bold",
                      marginTop: "-10px",
                      marginLeft: "-10px",
                    }}
                    format={"HH:mm:ss"}
                    ticking={true}
                    interval={1000}
                  />
                </Col>
                <Col span={12}></Col>
              </Row>
            </Card>
          </Col>
        </Row>
        {/* Chart */}
        <Row gutter={24}>
          <Col span={16}>
            <Card hoverable={true} style={{ marginTop: 16 }}>
              <Bar
                data={state}
                options={{
                  title: {
                    display: true,
                    text: "Equipment Usage",
                    fontSize: 20,
                  },
                  legend: {
                    display: true,
                    position: "right",
                  },
                }}
              />
            </Card>
          </Col>
          <Col span={8}>
            <img
              alt="iviva"
              style={{ height: "49%", marginTop: "7%" }}
              src="https://static.iviva.com/images/iviva_AboutUs-17.png"
            />
          </Col>
        </Row>
      </div>
    );
  }
}
