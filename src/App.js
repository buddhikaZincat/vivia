import React from "react";
import "./App.css";
import "./components/dashboard/Dashboard";
import { Dashboard } from "./components/dashboard/Dashboard";

function App() {
  return (
    <div className="container">
      <Dashboard />
    </div>
  );
}

export default App;
